<?php
// HTTP
define('HTTP_SERVER', 'http://apporio.org/hairshopee/admin/');
define('HTTP_CATALOG', 'http://apporio.org/hairshopee/');

// HTTPS
define('HTTPS_SERVER', 'http://apporio.org/hairshopee/admin/');
define('HTTPS_CATALOG', 'http://apporio.org/hairshopee/');

// DIR
define('DIR_APPLICATION', '/home/apporio/public_html/hairshopee/admin/');
define('DIR_SYSTEM', '/home/apporio/public_html/hairshopee/system/');
define('DIR_IMAGE', '/home/apporio/public_html/hairshopee/image/');
define('DIR_LANGUAGE', '/home/apporio/public_html/hairshopee/admin/language/');
define('DIR_TEMPLATE', '/home/apporio/public_html/hairshopee/admin/view/template/');
define('DIR_CONFIG', '/home/apporio/public_html/hairshopee/system/config/');
define('DIR_CACHE', '/home/apporio/public_html/hairshopee/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/apporio/public_html/hairshopee/system/storage/download/');
define('DIR_LOGS', '/home/apporio/public_html/hairshopee/system/storage/logs/');
define('DIR_MODIFICATION', '/home/apporio/public_html/hairshopee/system/storage/modification/');
define('DIR_UPLOAD', '/home/apporio/public_html/hairshopee/system/storage/upload/');
define('DIR_CATALOG', '/home/apporio/public_html/hairshopee/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'apporio_hairshop');
define('DB_PASSWORD', 'hairshopee@123!');
define('DB_DATABASE', 'apporio_hairshopee');
define('DB_PORT', '3306');
define('DB_PREFIX', 'apporio_');
